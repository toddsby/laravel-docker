<?php
 
class UserEmailTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->delete();
 
        User::create(array(
            'email' => 'first@user.net',
            'password' => Hash::make('pass1234')
        ));
 
        User::create(array(
            'email' => 'second@user.net',
            'password' => Hash::make('pass1234')
        ));
    }
 
}