<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$users = User::all();

		\FB::info($users);

	    return Response::json(array(
	        'error' => false,
	        'users' => $users->toArray()),
	        200
	    ); 
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$user = new User;

		//var_dump(Request::get('password'));

	    $user->email = Request::get('email');
	    $user->password = Request::get('password');

	    //print_r($user);

	    $success = $user->updateUniques();

	    if ($success) {
	    	return Response::json(array(
	    		'error' => false,
	        	'user' => $user->toArray()),
	        	200
	    	);
	    } else {
	    	//var_dump($user->errors()->all());
	    	return Response::json(array(
	    		'error' => true,
	        	'message' => $user->errors()->all()),
	        	200
	    	);
	    }

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
