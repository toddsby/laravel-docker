<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;

class User extends Ardent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Protect these fields from mass assignment
	 *
	 * @var array
	 */
	protected $guarded = array('id', 'password');

	/**
	 * Attributes excluded from JSON response
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public static $passwordAttributes  = array('password');
  	public $autoHashPasswordAttributes = true;

	/**
	 * Ardent validation rules
	 *
	 * @var array
	 */
	public static $rules = array(
    	'email'                 => 'required|email|unique',
    	'password'              => array('required','regex: @(?=[#$-/:-?{-~!"^_`\[\]a-zA-Z]*([0-9#$-/:-?{-~!"^_`\[\]]))(?=[#$-/:-?{-~!"^_`\[\]a-zA-Z0-9]*[a-zA-Z])[#$-/:-?{-~!"^_`\[\]a-zA-Z0-9]{4,}@')
  	);

  	public static $customMessages = array(
  		'password.regex' => 'Weak :attribute, must contain minimum of (4) characters consisting of at least (1) letter and (1) digit or (1) special character (?.#$)'
  	);

}
